/*
Folder and Files Preparation: 

1. Create a "readingListAct" Folder in your Batch Folder. 
2. Create an index.html and index.js file. For the index.html title just write "Reading List Activity 1" 
3. Show your solutions for each problem in the index.js file. 
4. Once done, Create a remote repository named "readingListAct".
5. Save your repo link in Boodle: WDC028V1.5b-18-C1 | JavaScript - Function Parameters, Return Statement and Array Manipulations
*/


// Activity Template:
// (Copy this part on your template)


/*
	1.) Create a function that returns a passed string with letters in alphabetical order.
		Example string: 'mastermind'
		Expected output: 'adeimmnrst'
	
*/

// Code here:
console.log('Number 1')

let sortAlphabets = function(text) {
        return text.split('').sort().join('');
    };
    console.log('This will convert string to array and sort the array then convert the array to a string')
    console.log(sortAlphabets('mastermind'));

console.log ('')
function sortAlphabet(str) {
        return [...str].sort((a, b) => a.localeCompare(b)).join("");
      }
      console.log("using the Arrow Function");
      console.log(sortAlphabet("mastermind"));

// console.log('')
// function setInAlphabeticalOrder(string){
//     let tester = 'abcdefghijklmnopqrstuvwxyz';
//     let alphabeticalOrder = '';

//     for(let x = 0; x < tester.length; x++){
//         for (let y = 0; y < string.length; y++){
//             if(string[y].toLowerCase() === tester[x].toLowerCase()){
//                 alphabeticalOrder += string [y]
//             }
//         }
//     }
//     console.log(alphabeticalOrder);
// }
// setInAlphabeticalOrder('mastermind')
/*
	2.) Write a simple JavaScript program to join all elements of the following array into a string.
	Sample array : myColor = ["Red", "Green", "White", "Black"];
		Expected output: 
			Red,Green,White,Black
			Red,Green,White,Black
			Red+Green+White+Black
*/

// Code here:
console.log('')
console.log('Number 2')

let myColor = ["Red", "Green", "White", "Black"];

let colors = myColor.toString()
    console.log('using the toString method')
    console.log(colors);

console.log('')
    colors = myColor.join(",")
    console.log('using the join method')
    console.log(colors);

console.log('')
myColor = ["Red", "Green", "White", "Black"].join("+")
    console.log('using the join method that is added at the end of the array')
    console.log(myColor);


/*
	3.) Write a function named birthdayGift that pass a gift as an argument.
		- if the gift is a stuffed toy, return a message that says: "Thank you for the stuffed toy, Michael!"
		- if the gift is a doll, return a message that says: "Thank you for the doll, Sarah!"
		- if the gift is a cake, return a message that says: "Thank you for the cake, Donna!"
		- if other gifts return a message that says: "Thank you for the (gift), Dad!"
		- create a global variable named myGift and invoke the function in the variable.
		- log the global var in the console

*/

// Code here:

// const myGift = birthdayGift();
console.log('')
console.log('Number 3')

function myGift(gift){
    
    if (gift === 'stuffed toy'){
        return `Thank you for the ${gift}, Michael!`
    } else if (gift === 'doll'){
        return `Thank you for the ${gift}, Sarah!`
    } else if (gift === 'cake'){
        return `Thank you for the ${gift}, Donna!`
    } else {
        return `Thank you for the ${gift}, Dad!`
    }
}

console.log(myGift('stuffed toy'))
console.log(myGift('doll'))
console.log(myGift('cake'))
console.log(myGift('BMW'))

// console.log('')
// console.log (`using switch method`)

// let birthdayGift = myGift1 ();

// function myGift1(gift){
//     switch(gift){
//         case 'stuffed toy':
//             console.log ('Thank you for the '+ gift +', Michael!');
//             break;
//         case 'doll':
//             console.log ('Thank you for the '+ gift +', Sarah!');
//             break;
//         case 'cake':
//             console.log ('Thank you for the '+ gift +', Donna!');
//             break;
//         default:
//             console.log ('Thank you for the '+ gift +', Dad!');
//             break;
//     }
// }

// console.log(myGift1('stuffed toy'))
// console.log(myGift1('doll'))
// console.log(myGift1('cake'))
// console.log(myGift1('BMW'))


/*4.) Write a function that accepts a string as a parameter and counts the number of vowels within the string.
		Example string: 'The quick brown fox'
		Expected Output: 5
        */

// Code here:
console.log('')
console.log('Number 4')

function vowelCount(vowel)
{
  let vowelList = 'aeiou';
  let vCount = 0;
  for(let x = 0; x < vowel.length ; x++)
  {
    if (vowelList.indexOf(vowel[x]) !== -1)
    {
      vCount += 1;
    }
  }
  return vCount;
}
console.log ('Count the number of vowel')
console.log(vowelCount("The quick brown fox"));

console.log('')
function countVouwel(myVouwel){
    let y = 0;
    for (let x = 0; x < myVouwel.length; x++){
	    if(
            myVouwel[x] == 'a' ||
            myVouwel[x] == 'e' ||
            myVouwel[x] == 'i' ||
            myVouwel[x] == 'o' ||
            myVouwel[x] == 'u'){
            y++;
	    }
    }
    console.log(y)
}

countVouwel('The quick brown fox')

